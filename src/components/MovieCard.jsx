import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import "./styles/MovieCard.css";
import { useNavigate } from 'react-router-dom';
import { MovieList } from "./MovieList"
import DescriptionPage from './DescriptionPage';

function MovieCard({ movie = {} }) {
    console.log("Movie in MovieCard", movie);
    const { TitleFilm, lienURL, rate } = movie
    return (
            <Card className="MovieCard" style={{ width: '18rem', display : 'flex' }}>
                <Card.Img variant="top" src={lienURL} />
                <Card.Body>
                    <Card.Title>{TitleFilm}</Card.Title>
                    <Card.Text> Note attribué : {rate} / 10 </Card.Text>
                    <a href={lienURL}><Button variant="primary">Regarder le film</Button></a>
                    <Button variant="primary"  onClick={navigate('/`description')}>Go to description</Button>
                </Card.Body>
            </Card>
    );
}

export default MovieCard;