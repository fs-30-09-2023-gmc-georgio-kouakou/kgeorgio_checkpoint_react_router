import "./styles/HeroCustom.css";
import ImageHeroCustom from "./img/movie.jpeg"

const HeroCustom = () => {
    return(
        <div className="d-flex justify-content-center align-items-center HeroCustom">
            <img className="HeroCustom--img" src={ImageHeroCustom} alt="" />
            <div className="HeroCustom--divText">
                <h1>Votre regal, notre passion</h1>
            </div>
        </div>
    )
}

export default HeroCustom;