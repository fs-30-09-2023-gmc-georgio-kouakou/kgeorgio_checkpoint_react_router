import MovieCard from "./MovieCard"
import "./styles/MovieList.css"

export const MovieList = ({movies}) =>{
    return(
        <div className="MovieList">
            {
                movies?.length>0 && movies?.map((movie,key) =>
                <div key={key} style={{margin : "1rem"}}>
                    <MovieCard movie={movie}/>
                </div>)
            
            }
        </div>
    )
}