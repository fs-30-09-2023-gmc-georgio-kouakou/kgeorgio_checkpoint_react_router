import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import "./styles/AddMovie.css";
import { useRef, useState } from 'react';
import { useEffect } from 'react';
import { MovieList } from './MovieList';

const AddMovie = () => {
    const [formValues, setFormValues] = useState({
        TitleFilm: "",
        lienURL: "",
        rate: "",
        description: ""
    })

    const [movies, setMovies]= useState([]);

    const handleChange = (event) => {
        const { name, value } = event.target;
        console.log(name, value);

        setFormValues({ ...formValues, [name]: value });
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(formValues);
        setMovies([...movies,formValues]);
    }

    return (
        <>
            <MovieList style={{display : 'flex'}} movies={movies}/>
            

            <hr />
            <Form className="container" onSubmit={handleSubmit}>
                <h1 className="Form--title">Ajouter un Film</h1>
                <Form.Group className="mb-3 mt-3" controlId="formBasicText">
                    <Form.Label>Nom du film</Form.Label>
                    <Form.Control type="text" placeholder="Entrez le nom du film" name="TitleFilm"
                        value={formValues?.TitleFilm}
                        onChange={handleChange} />
                </Form.Group>

                <Form.Group className="mb-3 mt-3" controlId="formBasicText_2">
                    <Form.Label>L'image du film</Form.Label>
                    <Form.Control type="text" placeholder="Entrez le lien URL du film" name="lienURL"
                        value={formValues?.lienURL}
                        onChange={handleChange} />
                </Form.Group>

                <Form.Group className="mb-3 mt-3" controlId="formBasicText_2">
                    <Form.Label>L'image du film</Form.Label>
                    <Form.Control as="textarea" rows={3} placeholder="Entrez le lien URL du film" name="lienURL"
                        value={formValues?.description}
                        onChange={handleChange} />
                </Form.Group>


                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Note attribué</Form.Label>
                    <Form.Control type="number" placeholder="Entrez la note attribuée à ce film sur 10" name="rate"
                        value={formValues?.rate}
                        onChange={handleChange} />
                </Form.Group>
                {/* <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group> */}
                <Button variant="primary" type="submit">
                    Soumettre le film
                </Button>
            </Form>
        </>
    );
}

export default AddMovie;