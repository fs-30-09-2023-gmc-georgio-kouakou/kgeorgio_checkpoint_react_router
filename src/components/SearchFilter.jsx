import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import "./styles/SearchFilter.css"

const SearchFilter = () => {
    return (
        <div className="SearchFilter">
            <div>
                <InputGroup className="">
                    <InputGroup.Text id="basic-addon1">🔍</InputGroup.Text>
                    <Form.Control
                        placeholder="tapez un film, une note"
                        aria-label="Username"
                        aria-describedby="basic-addon1"
                    />
                </InputGroup>
            </div>
        </div>
    )
}

export default SearchFilter;